import ipaddress

from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_shorewall/django_cdstack_tpl_shorewall"

    shorewall_zones = list()
    shorewall_zones.append("net")

    shorewall_ifaces = list()

    shorewall_policies = list()

    shorewall_snats = list()

    for key in template_opts.keys():
        if key.startswith("network_iface_") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            # check zone name and add it to list
            if key_ident + "_fw_zone" in template_opts:
                shorewall_zone = template_opts[key_ident + "_fw_zone"]
            else:
                shorewall_zone = "net"

            if shorewall_zone not in shorewall_zones:
                shorewall_zones.append(shorewall_zone)

            # attach zones to interfaces
            shorewall_iface = dict()
            shorewall_iface["zone"] = shorewall_zone

            if key_ident + "_ppp_unit" in template_opts:
                shorewall_iface["iface"] = (
                    "ppp" + template_opts[key_ident + "_ppp_unit"]
                )
            else:
                shorewall_iface["iface"] = network_iface

            if key_ident + "_fw_options" in template_opts:
                shorewall_iface["options"] = template_opts[key_ident + "_fw_options"]
            else:
                shorewall_iface["options"] = "routeback,dhcp"

            shorewall_ifaces.append(shorewall_iface)

            # enforce policies
            shorewall_policy_iface = list()

            if key_ident + "_fw_policy_allow" in template_opts:
                shorewall_policy_iface = (
                    template_opts[key_ident + "_fw_policy_allow"]
                    .replace(" ", "")
                    .split(",")
                )
            else:
                shorewall_policy_iface.append("net")

            for destination_zone in shorewall_policy_iface:
                if shorewall_zone == destination_zone:
                    continue

                duplicate = False

                for policy in shorewall_policies:
                    if (
                        policy["src_zone"] == shorewall_zone
                        and policy["dst_zone"] == destination_zone
                    ):
                        duplicate = True

                if shorewall_zone == "net":
                    continue

                if not duplicate:
                    policy = dict()
                    policy["src_zone"] = shorewall_zone
                    policy["dst_zone"] = destination_zone
                    policy["policy"] = "ACCEPT"

                    shorewall_policies.append(policy)

            # configure masquerading
            if key_ident + "_fw_snat_to" in template_opts and (
                key_ident + "_ip" in template_opts
                or key_ident + "_ip6" in template_opts
            ):
                shorewall_snat_ifaces = (
                    template_opts[key_ident + "_fw_snat_to"].replace(" ", "").split(",")
                )

                for ip_protocol in ("ipv4", "ipv6"):
                    if ip_protocol == "ipv4":
                        ip_address_key = ""
                    elif ip_protocol == "ipv6":
                        ip_address_key = "6"
                    else:
                        continue

                    if key_ident + "_ip" + ip_address_key in template_opts:
                        network = ipaddress.ip_network(
                            template_opts[key_ident + "_ip" + ip_address_key]
                            + "/"
                            + template_opts[key_ident + "_netmask" + ip_address_key],
                            strict=False,
                        )

                        # do not nat public ipv6
                        # TODO: add switch
                        if ip_protocol == "ipv6":
                            if not network.is_private:
                                continue

                        for snat_iface in shorewall_snat_ifaces:
                            snat_masq = dict()
                            snat_masq["action"] = "MASQUERADE"
                            snat_masq["source"] = str(network)
                            snat_masq["destination"] = snat_iface
                            snat_masq["ip_protocol"] = ip_protocol

                            shorewall_snats.append(snat_masq)

    # apply foreign snat
    for key in template_opts.keys():
        if key.startswith("firewall_foreign_snat_") and key.endswith("_source_network"):
            key_ident = key[:-15]

            network = ipaddress.ip_network(
                template_opts[key_ident + "_source_network"], strict=False
            )

            if isinstance(network, ipaddress.IPv4Network):
                ip_protocol = "ipv4"
            elif isinstance(network, ipaddress.IPv4Network):
                ip_protocol = "ipv6"
            else:
                continue

            shorewall_snat_ifaces = (
                template_opts[key_ident + "_fw_snat_to"].replace(" ", "").split(",")
            )

            for snat_iface in shorewall_snat_ifaces:
                snat_masq = dict()
                snat_masq["action"] = "MASQUERADE"
                snat_masq["source"] = str(network)
                snat_masq["ip_protocol"] = ip_protocol
                snat_masq["destination"] = snat_iface

                shorewall_snats.append(snat_masq)

    # apply rules
    shorewall_rules = list()

    for key in template_opts.keys():
        if key.startswith("firewall_rule_dnat_") and key.endswith("_source_zone"):
            key_ident = key[:-12]

            shorewall_rule = dict()
            shorewall_rule["action"] = "DNAT"
            shorewall_rule["ip_protocol"] = "ipv4"
            shorewall_rule["source_zone"] = template_opts[key_ident + "_source_zone"]

            if key_ident + "_source_host" in template_opts:
                shorewall_rule["source_host"] = template_opts[
                    key_ident + "_source_host"
                ]

            shorewall_rule["dest_zone"] = template_opts[key_ident + "_dest_zone"]
            shorewall_rule["dest_host"] = template_opts[key_ident + "_dest_host"]
            shorewall_rule["dest_port"] = template_opts[key_ident + "_dest_port"]
            shorewall_rule["proto"] = template_opts[key_ident + "_proto"]
            shorewall_rule["local_port"] = template_opts[key_ident + "_local_port"]

            shorewall_rules.append(shorewall_rule)

        if key.startswith("firewall_rule_port_") and key.endswith("_source_zone"):
            key_ident = key[:-12]

            shorewall_rule = dict()
            shorewall_rule["action"] = "ACCEPT"
            shorewall_rule["ip_protocol"] = "both"
            shorewall_rule["source_zone"] = template_opts[key_ident + "_source_zone"]

            if key_ident + "_source_host" in template_opts:
                shorewall_rule["source_host"] = template_opts[
                    key_ident + "_source_host"
                ]
                ip_obj = ipaddress.ip_address(shorewall_rule["source_host"])

                if isinstance(ip_obj, ipaddress.IPv4Address):
                    shorewall_rule["ip_protocol"] = "ipv4"
                elif isinstance(ip_obj, ipaddress.IPv6Address):
                    shorewall_rule["ip_protocol"] = "ipv6"
                else:
                    pass

            shorewall_rule["dest_zone"] = "fw"
            shorewall_rule["proto"] = template_opts[key_ident + "_proto"]
            shorewall_rule["local_port"] = template_opts[key_ident + "_local_port"]

            shorewall_rules.append(shorewall_rule)

    # apply exposed host rules
    shorewall_rules_exposed_hosts = list()

    for key in template_opts.keys():
        if key.startswith("firewall_rule_exposed_host_") and key.endswith(
            "_source_zone"
        ):
            key_ident = key[:-12]

            shorewall_rules_exposed_host = dict()
            shorewall_rules_exposed_host["source_zone"] = template_opts[
                key_ident + "_source_zone"
            ]

            if key_ident + "_source_host" in template_opts:
                shorewall_rules_exposed_host["source_host"] = template_opts[
                    key_ident + "_source_host"
                ]

            shorewall_rules_exposed_host["dest_zone"] = template_opts[
                key_ident + "_dest_zone"
            ]
            shorewall_rules_exposed_host["dest_host"] = template_opts[
                key_ident + "_dest_host"
            ]

            shorewall_rules_exposed_hosts.append(shorewall_rules_exposed_host)

    # IPsec tunnels
    shorewall_tunnels = list()

    if "ipsec_connections" in template_opts:
        for ipsec_connection in template_opts["ipsec_connections"]:
            shorewall_tunnel = dict()
            shorewall_tunnel["type"] = "ipsec"
            shorewall_tunnel["zone"] = ipsec_connection["ipsec_con_fwzone"]
            shorewall_tunnel["gateway"] = ipsec_connection["ipsec_con_right"]

            shorewall_tunnels.append(shorewall_tunnel)

    # custom setups
    for key in template_opts.keys():
        if key.startswith("firewall_zone_custom_") and key.endswith("_name"):
            key_ident = key[:-5]
            shorewall_zone = template_opts[key_ident + "_name"]

            # custom setups - add custom zone
            if shorewall_zone not in shorewall_zones:
                shorewall_zones.append(shorewall_zone)

            # custom setups - add custom interfaces
            if key_ident + "_ifaces" in template_opts:
                custom_interfaces = (
                    template_opts[key_ident + "_ifaces"].replace(" ", "").split(",")
                )

                for network_iface in custom_interfaces:
                    shorewall_iface = dict()
                    shorewall_iface["zone"] = shorewall_zone
                    shorewall_iface["iface"] = network_iface
                    shorewall_iface["options"] = "routeback"

                    shorewall_ifaces.append(shorewall_iface)

            # custom setups - add custom policy
            shorewall_policy_iface = list()

            if key_ident + "_fw_policy_allow" in template_opts:
                shorewall_policy_iface = (
                    template_opts[key_ident + "_fw_policy_allow"]
                    .replace(" ", "")
                    .split(",")
                )
            else:
                shorewall_policy_iface.append("net")

            for destination_zone in shorewall_policy_iface:
                if shorewall_zone == destination_zone:
                    continue

                duplicate = False

                for policy in shorewall_policies:
                    if (
                        policy["src_zone"] == shorewall_zone
                        and policy["dst_zone"] == destination_zone
                    ):
                        duplicate = True

                if not duplicate:
                    policy = dict()
                    policy["src_zone"] = shorewall_zone
                    policy["dst_zone"] = destination_zone
                    policy["policy"] = "ACCEPT"

                    shorewall_policies.append(policy)

    template_opts["shorewall_zones"] = shorewall_zones
    template_opts["shorewall_ifaces"] = shorewall_ifaces
    template_opts["shorewall_policies"] = shorewall_policies
    template_opts["shorewall_snats"] = shorewall_snats
    template_opts["shorewall_rules"] = shorewall_rules
    template_opts["shorewall_rules_exposed_hosts"] = shorewall_rules_exposed_hosts
    template_opts["shorewall_tunnels"] = shorewall_tunnels

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
